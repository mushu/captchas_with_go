//++++++++++++++++++++++++++++++++++++++++
//Fighting for great,share generate value!
//Build the best soft by golang,let's go!
//++++++++++++++++++++++++++++++++++++++++
//Author:ShirDon <http://www.shirdon.com>
//Email:hcbsts@163.com;  823923263@qq.com
//++++++++++++++++++++++++++++++++++++++++

package captchas_with_go

import (
	"testing"
	"time"
)

func TestMCStore(t *testing.T) {
	memcacheServers := []string{"127.0.0.1:11211"}
	store := CreateMCStore(100*time.Second, memcacheServers) //100 s

	captcha := new(CaptchaInfo)
	captcha.Text = "hello"
	captcha.CreateTime = time.Now()

	//test add and get
	key := store.Add(captcha)
	retV := store.Get(key)

	if retV.Text != captcha.Text {
		t.Errorf("not equal,retV:%#v,captcha:%#v", retV, captcha)
	}

	retV.Text = "world"
	store.Update(key, retV)
	retV = store.Get(key)

	if retV.Text != "world" {
		t.Errorf("update not equal,retV:%#v,captcha:%#v", retV, captcha)
	}

	t.Logf("TestMCStore:get from mc:%#v", retV)

	//test del
	store.Del(key)
	retV = store.Get(key)
	if nil != retV {
		t.Errorf("not del")
	}

}
